#variables defined
train_mass = 22680
train_acceleration = 10
train_distance = 100

bomb_mass = 1

#function to convert Farenheit to Celsius
def f_to_c(f_temp):
  c_temp = (f_temp-32) * (5/9)
  return c_temp
#function test
f100_in_celsius = f_to_c(100)
print(f100_in_celsius)

#function to convert celsius to farenheit 
def c_to_f(c_temp):
  f_temp = (c_temp * (9/5))+32
  return f_temp
#function test
c0_in_fahrenheit = c_to_f(0)
print(c0_in_fahrenheit)

#functon to get force
def get_force(mass, acceleration):
  return mass*acceleration
#function test
train_force = get_force(train_mass, train_acceleration)
print(train_force)

print("The GE train supplies " + str(train_force)+ " Newtons of force")

#function to get energy
def get_energy(mass, c=3*10**8):
  return mass*(c**2)
#function test
bomb_energy = get_energy(bomb_mass)
print("A 1 kg bomb supplies " + str(bomb_energy) + " joules. ")

#function to get work in joules 
def get_work(mass, acceleration, distance):
  force = get_force(mass, acceleration)
  work = force * distance
  return work
#function test
train_work = get_work(train_mass,train_acceleration, train_distance)
print("The GE train does " + str(train_work) + " joules of work over " + str(train_distance)+ " meters ")

  